export namespace Command {
	export const KH_KINGS_WIN: string = "kh_kings_win";
	export const KH_CHALLENGERS_WIN: string = "kh_challengers_win";
	export const KH_KINGS_CHICKEN: string = "kh_kings_chicken";
	export const KH_CHALLENGERS_CHICKEN: string = "kh_challengers_chicken";
	export const KH_MATCHMAKE: string = "kh_matchmake";
	export const KH_CLEAR_QUEUE: string = "kh_clear_queue";

	export const PL_ADD_NEW: string = "pl_add_new";
	export const PL_RENAME: string = "pl_rename";
	export const PL_DELETE: string = "pl_delete";
	export const PL_ENABLED_TOGGLE: string = "pl_enabled_toggle";
	export const PL_OP_TOGGLE: string = "pl_op_toggle";
	export const PL_SET_STATS: string = "pl_set_stats";

	export const ST_ALL_RESET_STATS: string = "st_all_reset_stats";
	export const ST_ALL_RESET_WINSTREAKS: string = "st_all_reset_winstreaks";
	export const ST_ALL_RESET_CHICKENS: string = "st_all_reset_chickens";
}