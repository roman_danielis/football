import { Team } from "../team"; require("../team");
import { Matchmaker } from "./matchmaker"; require("./matchmaker");
import { Player } from "../player"; require("../player");
import { Position } from "../enum"; require("../enum");
import { Serializable } from "../serialization/serializable"; require("../serialization/serializable");
import { Archive, SerializableType } from "../serialization/archive"; require("../serialization/archive");

export class KingOfTheHill extends Serializable {
	players: Player[] = [];
	queue: Team[] = [];
	current_streak: number = 0;

	private matchmaker: Matchmaker = new Matchmaker();

	constructor() {
		super();
	}

	get_player_by_name(name: string): Player {
		for (let i = 0; i < this.players.length; i++) {
			if (this.players[i].name === name) {
				return this.players[i];
			}
		}
		return null;
	}

	get_handicap(): number {
		return Math.min(Math.floor(this.current_streak / 2), 9);
	}

	matchmake(): void {
		if (this.queue.length === 0) {
			this.current_streak = 0;
		}

		let used_players: Set<Player> = new Set<Player>();
		for (let i = 0; i < this.queue.length; i++) {
			used_players.add(this.queue[i].goalie);
			used_players.add(this.queue[i].forward);
		}
		let free_players: Player[] = [];
		for (let i = 0; i < this.players.length; i++) {
			if (!used_players.has(this.players[i])) {
				free_players.push(this.players[i]);
			}
		}
		let new_teams: Team[] = this.matchmaker.get_teams(free_players);
		for (let i = 0; i < new_teams.length; i++) {
			this.queue.push(new_teams[i]);
		}
	}

	clear_queue(): void {
		this.queue.length = 0;
		this.current_streak = 0;
	}

	kings_win(): void {
		if (this.queue.length >= 2) {
			this.win(this.queue[0], this.queue[1]);
			this.queue.splice(1, 1);
		}
		if (this.queue.length < 2) {
			this.matchmake();
		}
	}

	challengers_win(): void {
		if (this.queue.length >= 2) {
			this.current_streak = 0;
			this.win(this.queue[1], this.queue[0]);
			this.queue.splice(0, 1);
		}
		if (this.queue.length < 2) {
			this.matchmake();
		}
	}

	kings_chicken(): void {
		if (this.queue.length) {
			this.current_streak = 0;
			this.chicken(this.queue[0]);
			this.queue.splice(0, 1);
		}
		if (this.queue.length < 2) {
			this.matchmake();
		}
	}

	challengers_chicken(): void {
		if (this.queue.length) {
			this.chicken(this.queue[1]);
			this.queue.splice(1, 1);
		}
		if (this.queue.length < 2) {
			this.matchmake();
		}
	}

	private win(winners: Team, losers: Team): void {
		this.current_streak++;

		winners.goalie.add_win(Position.Goalie, this.current_streak);
		winners.forward.add_win(Position.Forward, this.current_streak);

		losers.goalie.add_loss(Position.Goalie);
		losers.forward.add_loss(Position.Forward);
	}

	private chicken(team: Team): void {
		team.goalie.add_chicken();
		team.forward.add_chicken();
	}

	reset_all_stats(): void {
		this.current_streak = 0;
		this.clear_queue();
		for (let i = 0; i < this.players.length; i++) {
			this.players[i].reset_stats();
		}
	}

	reset_winstreaks(): void {
		this.current_streak = 0;
		for (let i = 0; i < this.players.length; i++) {
			this.players[i].reset_winstreaks();
		}
	}

	reset_chickens(): void {
		for (let i = 0; i < this.players.length; i++) {
			this.players[i].reset_chickens();
		}
	}

	add_player(name: string): boolean {
		for (let i = 0; i < this.players.length; i++) {
			if (this.players[i].name === name) {
				return false;
			}
		}
		let new_one: Player = new Player();
		new_one.name = name;
		this.players.push(new_one);
		return true;
	}

	private remove_team(index: number): void {
		this.queue.splice(index, 1);
		if (index === 0) {
			this.current_streak = 0;
		}
	}

	remove_player(player: Player): void {
		for (let i = 0; i < this.players.length; i++) {
			if (this.players[i] === player) {
				this.players.splice(i, 1);
				break;
			}
		}

		for (let i = this.queue.length - 1; i >= 0; i--) {
			if (this.queue[i].has_player(player)) {
				this.remove_team(i);
			}
		}
	}

	player_toggle_enabled(player: Player): void {
		player.enabled = !player.enabled;
		if (!player.enabled) {
			for (let i = this.queue.length - 1; i >= 0; i--) {
				if (this.queue[i].has_player(player)) {
					this.remove_team(i);
				}
			}
		}
	}

	serialize(archive: Archive): void {
		this.players = archive.do(this.players);
		this.queue = archive.do(this.queue);
		this.current_streak = archive.do(this.current_streak);
	}
	get_class(): SerializableType {
		return KingOfTheHill;
	}
}
Archive.register("KingOfTheHill", KingOfTheHill, 0);
