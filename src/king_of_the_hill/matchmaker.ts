import { Player } from "../player"; require("../player");
import { Team } from "../team"; require("../team");

export class Matchmaker {
	get_teams(players: Player[]): Team[] {
		let enabled_players: Player[] = players.filter((player: Player) => {
			return player.enabled;
		});

		let players_with_chicken: Player[] = [];
		for (let i = 0; i < players.length; i++) {
			if (players[i].get_chickens()) {
				players_with_chicken.push(players[i]);
			}
		}
		if (enabled_players.length % 2 === 1) {
			if (players_with_chicken.length) {
				shuffle(players_with_chicken);
				enabled_players.splice(enabled_players.indexOf(players_with_chicken[0]), 1);
				players_with_chicken[0].remove_chicken();
			}
		} else {
			if (players_with_chicken.length > 1) {
				shuffle(players_with_chicken);
				enabled_players.splice(enabled_players.indexOf(players_with_chicken[0]), 1);
				players_with_chicken[0].remove_chicken();
				enabled_players.splice(enabled_players.indexOf(players_with_chicken[1]), 1);
				players_with_chicken[1].remove_chicken();
			}
		}

		if (enabled_players.length % 2 === 1) {
			enabled_players.sort((a: Player, b: Player) => {
				return (a.stats_goalie.get_matches() + a.stats_forward.get_matches()) - (b.stats_goalie.get_matches() + b.stats_forward.get_matches());
			});
			enabled_players.pop();
		}

		enabled_players.sort((a: Player, b: Player) => {
			return a.get_sorting_winrate() - b.get_sorting_winrate();
		});

		let top_players: Player[] = enabled_players.slice(0, enabled_players.length / 2);
		let bottom_players: Player[] = enabled_players.slice(enabled_players.length / 2);

		function shuffle<T>(arr: T[]): void {
			for (let i = arr.length; i; i--) {
				let j = Math.floor(Math.random() * i);
				[arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
			}
		}

		shuffle(top_players);
		shuffle(bottom_players);

		let teams: Team[] = [];

		for (let i = 0; i < top_players.length; i++) {
			let team: Team = new Team(top_players[i], bottom_players[i]);
			if (team.goalie.get_goalie_ratio() === team.forward.get_goalie_ratio()) {
				if (Math.random() < 0.5) {
					team.swap();
				}
			} else if (team.goalie.get_goalie_ratio() > team.forward.get_goalie_ratio()) {
				team.swap();
			}
			teams.push(team);
		}

		return teams;
	}
}
