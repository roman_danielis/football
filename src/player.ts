import { Position } from "./enum"; require("./enum");
import { Serializable } from "./serialization/serializable"; require("./serialization/serializable");
import { Archive, SerializableType } from "./serialization/archive"; require("./serialization/archive");

export class Stats extends Serializable {
	wins: number = 0;
	loses: number = 0;
	longest_winstreak: number = 0;
	chickens: number = 0;

	constructor() {
		super();
	}

	get_matches(): number {
		return this.wins + this.loses;
	}

	get_winrate(): number {
		return this.get_matches() ? this.wins / this.get_matches() : 0;
	}

	reset(): void {
		this.wins = 0;
		this.loses = 0;
		this.longest_winstreak = 0;
		this.chickens = 0;
	}

	reset_winstreak(): void {
		this.longest_winstreak = 0;
	}

	reset_chickens(): void {
		this.chickens = 0;
	}

	serialize(archive: Archive): void {
		this.wins = archive.do(this.wins);
		this.loses = archive.do(this.loses);
		this.longest_winstreak = archive.do(this.longest_winstreak);
		this.chickens = archive.do(this.chickens);
	}
	get_class(): SerializableType {
		return Stats;
	}
}
Archive.register("Stats", Stats, 0);

export class Player extends Serializable {
	name: string = "";
	enabled: boolean = true;
	OP: boolean = false;

	stats_goalie: Stats = new Stats();
	stats_forward: Stats = new Stats();

	constructor() {
		super();
	}

	get_sorting_winrate(): number {
		if (this.OP) {
			return 2.0;
		} else {
			let wins = this.stats_goalie.wins + this.stats_forward.wins;
			let matches = this.stats_goalie.get_matches() + this.stats_forward.get_matches();
			return matches ? wins / matches : 0;
		}
	}
	get_goalie_ratio(): number {
		return this.stats_goalie.get_matches() / (this.stats_goalie.get_matches() + this.stats_forward.get_matches());
	}

	add_win(position: Position, newStreak: number): void {
		let stats: Stats = position === Position.Goalie ? this.stats_goalie : this.stats_forward;
		stats.wins++;
		stats.longest_winstreak = Math.max(stats.longest_winstreak, newStreak);
	}

	add_loss(position: Position): void {
		let stats: Stats = position === Position.Goalie ? this.stats_goalie : this.stats_forward;
		stats.loses++;
	}

	add_chicken(): void {
		this.stats_goalie.chickens++;
		this.stats_forward.chickens++;
	}

	remove_chicken(): void {
		this.stats_goalie.chickens = Math.max(0, this.stats_goalie.chickens - 1);
		this.stats_forward.chickens = Math.max(0, this.stats_forward.chickens - 1);
	}

	get_chickens(): number {
		return Math.max(this.stats_goalie.chickens, this.stats_forward.chickens);
	}

	reset_stats(): void {
		this.stats_goalie.reset();
		this.stats_forward.reset();
	}

	reset_winstreaks(): void {
		this.stats_goalie.reset_winstreak();
		this.stats_forward.reset_winstreak();
	}

	reset_chickens(): void {
		this.stats_goalie.reset_chickens();
		this.stats_forward.reset_chickens();
	}

	serialize(archive: Archive): void {
		this.name = archive.do(this.name);
		this.enabled = archive.do(this.enabled);
		this.OP = archive.do(this.OP);

		this.stats_goalie = archive.do(this.stats_goalie);
		this.stats_forward = archive.do(this.stats_forward);
	}
	get_class(): SerializableType {
		return Player;
	}
}
Archive.register("Player", Player, 0);
