export function wrap_to_admin_zone(zone_id: string, stuff: string): string {
	return "<div class='adminzone'>" +
		"<img src='./assets/admin.png' alt='' />" +
		"<b>A</b><b>d</b><b>m</b><b>i</b><b>n</b><b> </b><b>Z</b><b>o</b><b>n</b><b>e</b></div>" +
		"<form method='post' class='adminzone' id='" + zone_id + "'>" +
		stuff +
		"</form>"
		;
}

export function cb(zone_id: string, command: string): string {
	return [
		"onclick='",
		"document.getElementById(\"" + zone_id + "_command" + "\").value = \"" + command + "\";",
		"document.getElementById(\"" + zone_id + "\").submit();",
		"'",
	].join(" ");
}