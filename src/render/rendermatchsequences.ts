import { wrap_to_admin_zone, cb } from "./common"; require("./common");
import { Team } from "../team"; require("../team");
import { Command } from "../commands";

export function render_match_sequence(teams: Team[], winstreak: number, handicap: number, admin: boolean): string {
	let result: string = "";
	result += "<h2>Matches</h2>";
	result += "<table class='matches'>";
	result += "<p>Current win streak: ";
	for (let i = 0; i < winstreak; i++) {
		result += "<img src='./assets/crown_icon.png' alt='*' /> ";
	}
	result += "<br />Handicap: ";
	for (let i = 0; i < handicap; i++) {
		result += "<img src='./assets/handicap_icon.png' alt='o' /> ";
	}
	result += "</p>";
	result += "<tr>";
	result += "<th>Position</th>";
	for (let i = 0; i < teams.length; i++) {
		result += "<th>";
		if (i === 0) {
			result += "Kings";
		} else if (i === 1) {
			result += "Challengers";
		} else {
			result += i.toString();
		}
		result += "</th>";
	}
	result += "</tr>";
	result += "<tr>";
	result += "<td>Goalie</td>";
	for (let i = 0; i < teams.length; i++) {
		result += "<td>" + teams[i].goalie.name + "</td>";
	}
	result += "</tr>";
	result += "<tr>";
	result += "<td>Forward</td>";
	for (let i = 0; i < teams.length; i++) {
		result += "<td>" + teams[i].forward.name + "</td>";
	}
	result += "</tr>";
	result += "</table>";

	if (admin) {
		let zone_id: string = "king_sequence";
		let admin_zone: string = "";
		admin_zone += "<input type='button' value='Kings win' class='king_action' " + cb(zone_id, Command.KH_KINGS_WIN) + " />";
		admin_zone += "<input type='button' value='Challengers win' class='challenger_action' " + cb(zone_id, Command.KH_CHALLENGERS_WIN) + " />";
		admin_zone += "<br />";
		admin_zone += "<input type='button' value='Kings chicken' class='king_action' " + cb(zone_id, Command.KH_KINGS_CHICKEN) + " />";
		admin_zone += "<input type='button' value='Challengers chicken' class='challenger_action' " + cb(zone_id, Command.KH_CHALLENGERS_CHICKEN) + " />";
		admin_zone += "<br />";
		admin_zone += "<input type='button' value='Run matchmaker' " + cb(zone_id, Command.KH_MATCHMAKE) + " />";
		admin_zone += "<input type='button' value='Clear queue' " + cb(zone_id, Command.KH_CLEAR_QUEUE) + " />";
		admin_zone += "<br />";
		admin_zone += "<input id='" + zone_id + "_command' name='command' type='text' value='' hidden='true'>";
		admin_zone += "<input type='submit' hidden='true' />";
		result += wrap_to_admin_zone(zone_id, admin_zone);
	}

	return result;
}