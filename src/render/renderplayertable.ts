import { wrap_to_admin_zone, cb } from "./common"; require("./common");
import { Player, Stats } from "../player"; require("../player");
import { Command } from "../commands";

export function render_player_table(players: Player[], admin: boolean): string {
	let result: string = "";
	result += "<h2>Statistics</h2>";
	result += "<table class='stats'>";
	result += "<tr><th></th><th></th><th colspan='5'>Total</th><th colspan='5'>As goalie</th><th colspan='5'>As forward</th><th></th></tr>";
	result += "<tr><th>Name</th><th><img src='./assets/op.png' alt='OP' title='OP, will not play with other OP' /></th>";
	for (let i = 0; i < 3; i++) {
		result += "<th>Win%</th>";
		result += "<th>Wins</th>";
		result += "<th>Loses</th>";
		result += "<th>Games</th>";
		result += "<th><img src='./assets/crown_icon_black.png' alt='Longest reign' title='Longest reign as king' /></th>";
	}
	result += "<th><img src='./assets/chicken.png' alt='CO' title='Times chickened out' /></th>";
	result += "</tr>";


	for (let i = 0; i < players.length; i++) {
		if (!players[i].enabled) {
			result += "<tr class='disabledplayer'>";
		} else {
			result += "<tr>";
		}
		result += "<td>" + players[i].name + "</td>";
		result += "<td>";
		if (players[i].OP) {
			result += "<img src='./assets/op.png' alt='OP' title='" + players[i].name + " imba, plz nerf' />";
		}
		result += "</td>";
		for (let j = 0; j < 3; j++) {
			let stats: Stats = null;
			if (j === 0) {
				stats = new Stats();
				stats.wins = players[i].stats_goalie.wins + players[i].stats_forward.wins;
				stats.loses = players[i].stats_goalie.loses + players[i].stats_forward.loses;
				stats.longest_winstreak = Math.max(players[i].stats_goalie.longest_winstreak, players[i].stats_forward.longest_winstreak);
			}
			if (j === 1) { stats = players[i].stats_goalie; }
			if (j === 2) { stats = players[i].stats_forward; }
			let winrate: number = stats.get_winrate();
			result += "<td>" + Math.round(winrate * 100).toString() + "%</td>";
			result += "<td>" + stats.wins.toString() + "</td>";
			result += "<td>" + stats.loses.toString() + "</td>";
			result += "<td>" + stats.get_matches().toString() + "</td>";
			result += "<td>" + stats.longest_winstreak.toString() + "</td>";
		}
		result += "<td>" + (players[i].get_chickens()).toString() + "</td>";
		result += "</tr>";
	}
	result += "</table>";

	if (admin) {
		let zone_id: string = "player_stats";
		let admin_zone: string = "";
		admin_zone += "<input name='param1' type='text' value='player name' />";
		admin_zone += "<input type='button' value='Add new' " + cb(zone_id, Command.PL_ADD_NEW) + " />/";
		admin_zone += "<input type='button' value='Rename to' " + cb(zone_id, Command.PL_RENAME) + " />";
		admin_zone += "<input id='" + zone_id + "_param2' name='param2' type='text' value='new player name' />";
		admin_zone += "<br />";
		admin_zone += "<input type='button' value='Enabled/Disabled' " + cb(zone_id, Command.PL_ENABLED_TOGGLE) + " />";
		admin_zone += "<input type='button' value='OP/noob' " + cb(zone_id, Command.PL_OP_TOGGLE) + " />|";
		admin_zone += "<input type='button' value='DELETE' " + cb(zone_id, Command.PL_DELETE) + " />";
		admin_zone += "<input type='button' value='&lt;stats template&gt;' onclick='document.getElementById(\"" + zone_id + "_param2\").value=\"g,W,L,S|f,W,L,S\"' />";
		admin_zone += "<input type='button' value='Set stats' " + cb(zone_id, Command.PL_SET_STATS) + " />";
		admin_zone += "<h3>All</h3>"
		admin_zone += "<input type='button' value='Reset all stats' " + cb(zone_id, Command.ST_ALL_RESET_STATS) + " />";
		admin_zone += "<input type='button' value='Reset all winstreaks' " + cb(zone_id, Command.ST_ALL_RESET_WINSTREAKS) + " />";
		admin_zone += "<input type='button' value='Reset all chickens' " + cb(zone_id, Command.ST_ALL_RESET_CHICKENS) + " />";
		admin_zone += "<input id='" + zone_id + "_command' name='command' type='text' hidden='true' />";
		admin_zone += "<input type='submit' method='post' hidden='true' />";

		result += wrap_to_admin_zone(zone_id, admin_zone);
	}

	return result;
}
