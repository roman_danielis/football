import { Serializable } from "./serializable"; require("./serializable");

// Based on http://www.json.org/json2.js
// let cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
// let escapable = /[\\\'\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
// let meta: {[key: string]: string} = { // table of character substitutions
// 	'\b': '\\b',
// 	'\t': '\\t',
// 	'\n': '\\n',
// 	'\f': '\\f',
// 	'\r': '\\r',
// 	"'": "\\'",
// 	'\\': '\\\\',
// };
// function escapeValue(v: string) {
// 	escapable.lastIndex = 0;
// 	return escapable.test(v) ?
// 		"'" + v.replace(escapable, function (a) {
// 			var c: string = meta[a];
// 			return typeof c === 'string' ? c :
// 				'\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
// 		}) + "'" :
// 		"'" + v + "'";
// }


// TODO: test if \n replace has effect

// markings to indicate next type, assumed to be one character
const BOOLEAN_MARK: string = "b";
const NUMBER_MARK: string = "n";
const STRING_MARK: string = "s";
const OBJECT_MARK: string = "#";
const REFERENCE_MARK: string = "@";
const ARRAY_MARK: string = "a";
const SET_MARK: string = "t";
const MAP_MARK: string = "m";
const VERSION_SEPARATOR: string = "|";
const NULL_MARK: string = "-";
const UNDEFINED_MARK: string = "?";

export type SER_TYPE = Serializable | number | string | boolean | any[] | Set<any> | Map<any, any> | null | undefined;

export type SerializableType = any; // FIXME: can this have any better typing? *stuff extending Serializable

export abstract class Archive {
	private static registered_names: Map<SerializableType, string> = new Map<SerializableType, string>();
	private static registered_names_version: Map<SerializableType, number> = new Map<SerializableType, number>();
	private static registered_types: Map<string, SerializableType> = new Map<string, SerializableType>();

	private references: Map<number, any> = new Map<number, any>();
	private references_back: Map<any, number> = new Map<any, number>();

	protected data: string[] = [];
	protected data_read_index: number = 0;

	constructor() {

	}

	abstract do<Type extends SER_TYPE>(item: Type): Type;

	private solveReferenceIn<Type extends SER_TYPE>(item: Type, inFunction: () => void): void {
		let ref: number = this.references_back.get(item);
		if (ref === undefined) {
			let idx: number = this.references.size;
			this.references.set(idx, item);
			this.references_back.set(item, idx);
			inFunction();
		} else {
			this.data.push(REFERENCE_MARK + ref);
		}
	}

	protected in<Type extends SER_TYPE>(item: Type): void {
		if (this.data_read_index) {
			throw new Error("Archive.in: attempted write to archive when already reading");
		}
		if (typeof item === "boolean") {
			this.data.push(BOOLEAN_MARK);
			this.data.push((item as boolean) ? "1" : "0");
		} else if (typeof item === "number") {
			this.data.push(NUMBER_MARK);
			this.data.push((item as number).toString());
		} else if (typeof item === "string") {
			this.data.push(STRING_MARK);
			this.data.push((item as string).substring(0).replace("\n", "\\n"));
		} else if ((item instanceof Serializable)) {
			if (Archive.registered_names.get(item.get_class()) === undefined) {
				throw new Error("Archive.in: attempting to serialize Serializable, but this type is not registered: " + typeof item);
			}
			this.solveReferenceIn(item, () => {
				this.data.push(OBJECT_MARK + Archive.registered_names.get(item.get_class()) + VERSION_SEPARATOR + Archive.registered_names_version.get(item.get_class()));
				let huh: number = Archive.registered_names_version.get(item.get_class());
				item.serialize(this, huh);
			});

		} else if (Array.isArray(item)) {
			this.solveReferenceIn(item, () => {
				this.data.push(ARRAY_MARK + item.length.toString());
				for (let i = 0; i < item.length; i++) {
					this.in(item[i]);
				}
			});
		} else if (item instanceof Set) {
			this.solveReferenceIn(item, () => {
				this.data.push(SET_MARK + item.size.toString());
				for (let it of item) {
					this.in(it);
				}
			});
		} else if (item instanceof Map) {
			this.solveReferenceIn(item, () => {
				this.data.push(MAP_MARK + item.size.toString());
				for (let it of item) {
					this.in(it[0]);
					this.in(it[1]);
				}
			});

		} else if (item === null) {
			this.data.push(NULL_MARK);

		} else if (item === undefined) {
			this.data.push(UNDEFINED_MARK);

		} else {
			throw new Error("Archive.in: Unregistered type" + item);
		}
	}

	private addReference(item: any): void {
		let idx: number = this.references.size;
		this.references.set(idx, item);
		this.references_back.set(item, idx);
	}

	protected out(): any {
		if (this.data_read_index >= this.data.length) {
			throw new Error("Archive.out: overflow");
		}

		let type: string = this.data[this.data_read_index++];
		if (type[0] === BOOLEAN_MARK) {
			return parseInt(this.data[this.data_read_index++]) ? true : false;
		} else if (type[0] === NUMBER_MARK) {
			return parseFloat(this.data[this.data_read_index++]);
		} else if (type[0] === STRING_MARK) {
			return this.data[this.data_read_index++].replace("\\n", "\n");
		} else if (type[0] === OBJECT_MARK) {
			let class_type: string = type.substring(1, type.indexOf(VERSION_SEPARATOR));
			let classs = (Archive.registered_types.get(class_type) as any); // FUCK THE TYPE
			if (classs === undefined) {
				throw new Error("Archive.out: deserialization of object failed, type not registered: " + class_type);
			}
			let item: Serializable = new classs();
			this.addReference(item);
			item.serialize(this, parseInt(type.substr(type.indexOf(VERSION_SEPARATOR) + 1)));
			return item;
		} else if (type[0] === REFERENCE_MARK) {
			return this.references.get(parseInt(type.substring(1)));

		} else if (type[0] === ARRAY_MARK) {
			let size: number = parseInt(type.substring(1));
			let item: any[] = [];
			this.addReference(item);
			for (let i = 0; i < size; i++) {
				item.push(this.out());
			}
			return item;
		} else if (type[0] === SET_MARK) {
			let size: number = parseInt(type.substring(1));
			let item: Set<any> = new Set();
			this.addReference(item);
			for (let i = 0; i < size; i++) {
				item.add(this.out());
			}
			return item;
		} else if (type[0] === MAP_MARK) {
			let size: number = parseInt(type.substring(1));
			let item: Map<any, any> = new Map();
			this.addReference(item);
			for (let i = 0; i < size; i++) {
				item.set(this.out(), this.out());
			}
			return item;

		} else if (type[0] === NULL_MARK) {
			return null;

		} else if (type[0] === UNDEFINED_MARK) {
			return undefined;

		} else {
			throw new Error("Archive.out: unknown type " + type);
		}
	}

	// serializable is Class (not instance) that needs to be registered in order to be serialized or deserialized
	// it needs to be derived from Serializable and needs constructor that works with no parameters
	// if you know how enforce it in TypeScript, be my guest!
	static register(register_id: string, serializable: SerializableType, version: number): void {
		let v: typeof Serializable = Archive.registered_types.get(register_id);
		if (v) {
			throw new Error("Archive.register: \"" + register_id + "\" already registered");
		} else {
			Archive.registered_names.set(serializable, register_id);
			Archive.registered_names_version.set(serializable, version);
			Archive.registered_types.set(register_id, serializable);
		}
	}
}