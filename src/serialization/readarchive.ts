import { Archive, SER_TYPE } from "./archive"; require("./archive");

export class ReadArchive extends Archive {
    do<Type extends SER_TYPE>(item: Type): Type {
        return this.out();
    }

    load(serialized_string: string): void {
		if (this.data.length || this.data_read_index) {
			throw new Error("Archive.load: archive already contains data");
		}
		// this.data = serialized_string.split(/[-\\]1\n/g);
		this.data = serialized_string.split(/\n/g);
		// for (let i = 0; i < this.data.length; i++) {
		// 	this.data[i] = this.data[i].replace("\\n", "\n");
		// }
	}
}