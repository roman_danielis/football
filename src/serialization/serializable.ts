import { Archive, SerializableType } from "./archive"; require("./archive");

export abstract class Serializable {
	abstract serialize(archive: Archive, version: number): void;
	abstract get_class(): SerializableType;
}