import { Archive, SER_TYPE } from "./archive"; require("./archive");

export class WriteArchive extends Archive {
    do<Type extends SER_TYPE>(item: Type): Type {
        this.in(item);
        return item;
    }

    write(): string {
		return this.data.join("\n");
	}
}