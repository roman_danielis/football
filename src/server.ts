///<reference path="../node_modules/@types/node/index.d.ts"/>

import * as http from "http";
import * as fs from "fs";
import * as qs from "querystring";

import { KingOfTheHill } from "./king_of_the_hill/kingofthehill"; require("./king_of_the_hill/kingofthehill");
import { ReadArchive } from "./serialization/readarchive"; require("./serialization/readarchive");
import { WriteArchive } from "./serialization/writearchive"; require("./serialization/writearchive");
import { render_match_sequence } from "./render/rendermatchsequences"; require("./render/rendermatchsequences");
import { render_player_table } from "./render/renderplayertable"; require("./render/renderplayertable");
import { Command } from "./commands"; require("./commands");

function process_command(kings: KingOfTheHill, command: string, parameter: string, parameter2: string): string {
	let error_message: string = "";
	if (command === Command.KH_KINGS_WIN) {
		kings.kings_win();
	} else if (command === Command.KH_CHALLENGERS_WIN) {
		kings.challengers_win();
	} else if (command === Command.KH_KINGS_CHICKEN) {
		kings.kings_chicken();
	} else if (command === Command.KH_CHALLENGERS_CHICKEN) {
		kings.challengers_chicken();
	} else if (command === Command.KH_MATCHMAKE) {
		kings.matchmake();
	} else if (command === Command.KH_CLEAR_QUEUE) {
		kings.clear_queue();

	} else if (command === Command.PL_ADD_NEW) {
		if (!kings.add_player(parameter)) {
			error_message = "We prolly have that name there";
		}
	} else if (command === Command.PL_RENAME) {
		let player = kings.get_player_by_name(parameter);
		if (player) {
			player.name = parameter2;
		} else {
			error_message = "No player with that name";
		}
	} else if (command === Command.PL_DELETE) {
		let player = kings.get_player_by_name(parameter);
		if (player) {
			kings.remove_player(player);
		} else {
			error_message = "No player with that name";
		}
	} else if (command === Command.PL_ENABLED_TOGGLE) {
		let player = kings.get_player_by_name(parameter);
		if (player) {
			kings.player_toggle_enabled(player);
		} else {
			error_message = "No player with that name";
		}
	} else if (command === Command.PL_OP_TOGGLE) {
		let player = kings.get_player_by_name(parameter);
		if (player) {
			player.OP = !player.OP;
		} else {
			error_message = "No player with that name";
		}
	} else if (command === Command.PL_SET_STATS) {
		let player = kings.get_player_by_name(parameter);
		if (player) {
			let gf = parameter2.split("|");
			if (gf.length === 2) {
				player.reset_stats();
				let st = [
					gf[0].split(",").slice(1).map((it) => { return parseInt(it); }),
					gf[1].split(",").slice(1).map((it) => { return parseInt(it); })];
				let all_nums = true;
				for (let i = 0; i < st.length; i++) {
					for (let j = 0; j < st[i].length; j++) {
						all_nums = all_nums && !isNaN(st[i][j]);
					}
				}
				if (st[0].length === 3 && st[1].length === 3 && all_nums) {
					player.stats_goalie.wins = st[0][0];
					player.stats_goalie.loses = st[0][1];
					player.stats_goalie.longest_winstreak = st[0][2];

					player.stats_forward.wins = st[1][0];
					player.stats_forward.loses = st[1][1];
					player.stats_forward.longest_winstreak = st[1][2];
				} else {
					error_message = "Invalid format, split to w/l/g/s with ,";
				}
			} else {
				error_message = "Invalid format, split to g/f with |";
			}
		} else {
			error_message = "No player with that name";
		}

	} else if (command === Command.ST_ALL_RESET_STATS) {
		kings.reset_all_stats();
	} else if (command === Command.ST_ALL_RESET_WINSTREAKS) {
		kings.reset_winstreaks();
	} else if (command === Command.ST_ALL_RESET_CHICKENS) {
		kings.reset_chickens();

	} else {
		error_message = "Unknown command " + command;
	}

	return error_message;
}

function respond_with_page(king_of_the_hill: KingOfTheHill, response: http.ServerResponse, header_text: string, footer_text: string, admin: boolean): void {
	let rules_text: string = fs.readFileSync("html_templates/rules.html", "utf8");
	let matchmaking_text: string = fs.readFileSync("html_templates/matchmaking.html", "utf8");

	let queue_text: string = render_match_sequence(king_of_the_hill.queue, king_of_the_hill.current_streak, king_of_the_hill.get_handicap(), admin);
	let playertable_text: string = render_player_table(king_of_the_hill.players, admin);

	response.writeHead(200, { "Content-Type": "text/html" });
	response.end(header_text + queue_text + "<hr />" + playertable_text + "<hr />" + rules_text + "<hr />" + matchmaking_text + "<hr />" + footer_text);
}

const server: http.Server = http.createServer((request: http.IncomingMessage, response: http.ServerResponse) => {
	if (request.url.substr(0, 4) === "/css") {
		fs.readFile(request.url.substr(1), (err, data) => {
			response.writeHead(200, { "Content-Type": "text/css" });
			response.end(data);
		}); // FIXME: THIS NEEDS TO BE BETTER!

	} else if (request.url.substr(0, 7) === "/assets") {
		fs.readFile(request.url.substr(1), (err, data) => {
			response.writeHead(200, /*{ "Content-Type": "base64" }*/);
			response.end(data);
		});
	} else if (request.url === "/") {
		let king_of_the_hill: KingOfTheHill = new KingOfTheHill();
		let king_data: string = "";
		try {
			king_data = fs.readFileSync("data/kings.txt", "utf8");
		} catch (e) {
			console.log("No king data, creating default");
		}

		if (king_data !== "") {
			try {
				let read_archive: ReadArchive = new ReadArchive();
				read_archive.load(king_data);
				king_of_the_hill = read_archive.do(king_of_the_hill);
			} catch (e) {
				response.writeHead(200, { "Content-Type": "text/html" });
				response.end("<div class='badstuff'>Error deserializing</div>");
				return;
			}
		}

		let header_text: string = fs.readFileSync("html_templates/header.html", "utf8");
		let footer_text: string = fs.readFileSync("html_templates/footer.html", "utf8");
		if (king_of_the_hill) {

			if (request.method === "POST") {
				let body: string = "";
				request.on("data", (data) => {
					body += data;
					// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
					if (body.length > 1e6) {
						// FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
						request.connection.destroy();
					}
				});
				request.on("end", () => {
					let POST = qs.parse(body);
					// use POST
					if (POST["command"]) {
						let command = POST["command"];
						let param1 = POST["param1"];
						if (!param1) {
							param1 = "";
						}
						let param2 = POST["param2"];
						if (!param2) {
							param2 = "";
						}

						let error: string = process_command(king_of_the_hill, command, param1, param2);
						if (error === "") {
							try {
								let write_archive: WriteArchive = new WriteArchive();
								write_archive.do(king_of_the_hill);
								let new_king_data: string = write_archive.write();
								if (!fs.existsSync("data")) {
									fs.mkdirSync("data");
								}
								fs.writeFileSync("data/kings.txt", new_king_data, { encoding: "utf8" });

								// make a backup, losing data is scary bad thing
								let milliseconds = (new Date).getTime();
								fs.writeFileSync("data/kings" + milliseconds.toString() + ".txt", king_data, { encoding: "utf8" });
							} catch (e) {
								response.writeHead(200, { "Content-Type": "text/html" });
								response.end(header_text + "<div class='badstuff'>Error while saving new data</div>" + footer_text);
								return;
							}

							// response.writeHead(200, { "Content-Type": "text/html" });
							// response.end(header_text + "<div class='goodstuff'>The deed is done <a href='/'>Noice!</a></div>" + footer_text);
							respond_with_page(king_of_the_hill, response, header_text, footer_text, true);
						} else {
							response.writeHead(200, { "Content-Type": "text/html" });
							response.end(header_text + "<div class='badstuff'>" + error + "</div>" + footer_text);
						}
					} else if (POST["password"]) {
						if (POST["password"] === "¯\\_(ツ)_/¯") {
							respond_with_page(king_of_the_hill, response, header_text, footer_text, true);
						} else {
							response.writeHead(200, { "Content-Type": "text/html" });
							response.end(header_text + "<div class='badstuff'>(╯°□°）╯︵ ┻━┻</div>" + footer_text);
						}
					}
				});

			} else { // no command issued, just display the damn page
				respond_with_page(king_of_the_hill, response, header_text, footer_text, false);
			}

		} else {
			response.writeHead(200, { "Content-Type": "text/html" });
			response.end(header_text + "<div class='badstuff'>Something bad happened</div>" + footer_text);
		}
	} else {
		response.writeHead(400);
		response.end();
	}
});

const port = 8424;

server.listen(port);
console.log("Listening on http://localhost:" + port);
