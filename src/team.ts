import { Player } from "./player"; require("./player");
import { Serializable } from "./serialization/serializable"; require("./serialization/serializable");
import { Archive, SerializableType } from "./serialization/archive"; require("./serialization/archive");

export class Team extends Serializable {
	goalie: Player;
	forward: Player;

	constructor(goalie: Player = null, forward: Player = null) {
		super();

		this.goalie = goalie;
		this.forward = forward;
	}

	swap(): void {
		let sw: Player = this.goalie;
		this.goalie = this.forward;
		this.forward = sw;
	}

	has_player(player: Player): boolean {
		return this.goalie === player || this.forward === player;
	}

	serialize(archive: Archive): void {
		this.goalie = archive.do(this.goalie);
		this.forward = archive.do(this.forward);
	}
	get_class(): SerializableType {
		return Team;
	}
}
Archive.register("Team", Team, 0);